/*
- Создайте программу, определяющую, подходят ли текущие погодные условия для прогулки
- В начале работы программы пользователь вводит температуру окружающей среды, информацию о ветре и дожде
- Программа ее обрабатывает и дает совет
 */


import java.util.Scanner;

public class WeatherAnalyzer {
    public static void main(String[] args){
        double temperature, windSpeed;
        int precipitationProbability;
        Scanner scan = new Scanner(System.in);

        System.out.println("Определение погодных условий\n");

        System.out.print("Введите температуру окружающей среды (°C): ");
        temperature = scan.nextDouble();

        System.out.print("Введите скорость ветра (м/с): ");
        windSpeed = scan.nextDouble();

        System.out.print("Введите вероятность выпадения осадков в процентах: ");
        precipitationProbability = scan.nextInt();

        if (temperature <= 3.0 || temperature >= 34.0 || windSpeed >= 10.8 || precipitationProbability >= 40)
            System.out.println("\nСовет: на прогулку лучше не выходить");
        else
            System.out.println("\nСовет: можно выйти на прогулку");
    }
}
